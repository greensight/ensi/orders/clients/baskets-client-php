# Ensi\BasketsClient\CustomerBasketsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteBasketCustomer**](CustomerBasketsApi.md#deleteBasketCustomer) | **DELETE** /baskets/baskets/customer:delete | Delete the user&#39;s current basket
[**searchBasketCustomer**](CustomerBasketsApi.md#searchBasketCustomer) | **POST** /baskets/baskets/customer:search-one | Get information about the user&#39;s basket
[**setBasketCustomerItem**](CustomerBasketsApi.md#setBasketCustomerItem) | **POST** /baskets/baskets/customer:set-item | Set the product in the user&#39;s basket



## deleteBasketCustomer

> \Ensi\BasketsClient\Dto\EmptyDataResponse deleteBasketCustomer($delete_basket_customer_request)

Delete the user's current basket

Delete the user's current basket

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BasketsClient\Api\CustomerBasketsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$delete_basket_customer_request = new \Ensi\BasketsClient\Dto\DeleteBasketCustomerRequest(); // \Ensi\BasketsClient\Dto\DeleteBasketCustomerRequest | 

try {
    $result = $apiInstance->deleteBasketCustomer($delete_basket_customer_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerBasketsApi->deleteBasketCustomer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delete_basket_customer_request** | [**\Ensi\BasketsClient\Dto\DeleteBasketCustomerRequest**](../Model/DeleteBasketCustomerRequest.md)|  |

### Return type

[**\Ensi\BasketsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchBasketCustomer

> \Ensi\BasketsClient\Dto\SearchBasketCustomerResponse searchBasketCustomer($search_basket_customer_request)

Get information about the user's basket

Get information about the user's basket

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BasketsClient\Api\CustomerBasketsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_basket_customer_request = new \Ensi\BasketsClient\Dto\SearchBasketCustomerRequest(); // \Ensi\BasketsClient\Dto\SearchBasketCustomerRequest | 

try {
    $result = $apiInstance->searchBasketCustomer($search_basket_customer_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerBasketsApi->searchBasketCustomer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_basket_customer_request** | [**\Ensi\BasketsClient\Dto\SearchBasketCustomerRequest**](../Model/SearchBasketCustomerRequest.md)|  |

### Return type

[**\Ensi\BasketsClient\Dto\SearchBasketCustomerResponse**](../Model/SearchBasketCustomerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## setBasketCustomerItem

> \Ensi\BasketsClient\Dto\EmptyDataResponse setBasketCustomerItem($set_basket_item_request)

Set the product in the user's basket

Set the product in the user's basket

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BasketsClient\Api\CustomerBasketsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$set_basket_item_request = new \Ensi\BasketsClient\Dto\SetBasketItemRequest(); // \Ensi\BasketsClient\Dto\SetBasketItemRequest | 

try {
    $result = $apiInstance->setBasketCustomerItem($set_basket_item_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerBasketsApi->setBasketCustomerItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **set_basket_item_request** | [**\Ensi\BasketsClient\Dto\SetBasketItemRequest**](../Model/SetBasketItemRequest.md)|  |

### Return type

[**\Ensi\BasketsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

