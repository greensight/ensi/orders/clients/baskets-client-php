# Ensi\BasketsClient\BasketsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchBaskets**](BasketsApi.md#searchBaskets) | **POST** /baskets/baskets:search | Search for objects of the type Basket



## searchBaskets

> \Ensi\BasketsClient\Dto\SearchBasketsResponse searchBaskets($search_baskets_request)

Search for objects of the type Basket

Search for objects of the type Basket

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BasketsClient\Api\BasketsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_baskets_request = new \Ensi\BasketsClient\Dto\SearchBasketsRequest(); // \Ensi\BasketsClient\Dto\SearchBasketsRequest | 

try {
    $result = $apiInstance->searchBaskets($search_baskets_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BasketsApi->searchBaskets: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_baskets_request** | [**\Ensi\BasketsClient\Dto\SearchBasketsRequest**](../Model/SearchBasketsRequest.md)|  |

### Return type

[**\Ensi\BasketsClient\Dto\SearchBasketsResponse**](../Model/SearchBasketsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

