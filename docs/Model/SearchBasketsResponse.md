# # SearchBasketsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\BasketsClient\Dto\Basket[]**](Basket.md) |  | 
**meta** | [**\Ensi\BasketsClient\Dto\SearchBasketsResponseMeta**](SearchBasketsResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


