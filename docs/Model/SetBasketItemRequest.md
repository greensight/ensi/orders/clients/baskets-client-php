# # SetBasketItemRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer_id** | **int** | Client ID | 
**items** | [**\Ensi\BasketsClient\Dto\SetBasketItemRequestItems[]**](SetBasketItemRequestItems.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


