# # Basket

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Basket ID | 
**customer_id** | **int** | Customer ID | 
**created_at** | [**\DateTime**](\DateTime.md) | Date of creation | 
**updated_at** | [**\DateTime**](\DateTime.md) | Date of update | 
**promo_code** | **string** | The applied promo code | 
**items** | [**\Ensi\BasketsClient\Dto\BasketItem[]**](BasketItem.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


