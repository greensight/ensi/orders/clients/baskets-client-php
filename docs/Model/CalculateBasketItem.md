# # CalculateBasketItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offer_id** | **int** | ID оффера | 
**product_id** | **int** | ID товара | 
**name** | **string** | Название товара | 
**code** | **string** | Код товара (выводится в URL) | 
**barcode** | **string** | Штрихкод (EAN) | 
**vendor_code** | **string** | Артикул | [optional] 
**type** | **int** | Тип товара | 
**is_adult** | **bool** | Товар 18+ | 
**uom** | **int** | Единица измерения весового товара | 
**tariffing_volume** | **int** | Единица тарификации весового товара | 
**order_step** | **float** | Шаг пикера | 
**order_minvol** | **float** | Минимальное количество для заказа | 
**main_image** | **string** | URL основной картинки | 
**qty** | **float** | Кол-во товара | 
**stock_qty** | **int** | Сток по товару | 
**price** | **int** | Цена товара (цена * qty - скидки) (в коп.) | 
**price_per_one** | **int** | Цена единичного товара (в коп.) | 
**cost** | **int** | Цена товара до скидок (цена * qty) (в коп.) | 
**cost_per_one** | **int** | Цена единичного товара до скидок (в коп.) | 
**discount** | **int** | Скидка по товару (скидка * qty) (в коп.) | 
**discount_per_one** | **int** | Скидка единичного товара (в коп.) | 
**discounts** | [**\Ensi\BasketsClient\Dto\Discount[]**](Discount.md) |  | [optional] 
**images** | [**\Ensi\BasketsClient\Dto\ProductImage[]**](ProductImage.md) |  | [optional] 
**brand** | [**\Ensi\BasketsClient\Dto\Brand**](Brand.md) |  | [optional] 
**nameplates** | [**\Ensi\BasketsClient\Dto\Nameplate[]**](Nameplate.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


